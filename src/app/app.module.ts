import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { ShoppingListPage } from "../pages/shopping-list/shopping-list";
import { RecipesPage } from "../pages/recipes/recipes";
import { ViewRecipePage } from "../pages/recipes/view/recipe";
import { EditRecipePage } from "../pages/recipes/edit/recipe";
import { TabsPage } from "../pages/tabs/tabs";
import { RecipeService } from "../service/recipe";
import { ShoppingListService } from "../service/shopping-list";
import { SignInPage } from "../pages/user/signin";
import { SignUpPage } from "../pages/user/signup";
import { AuthService } from "../service/auth";
import { SLOptionsPage } from "../pages/shopping-list/sl-options/sl-options";
import { HttpModule } from "@angular/http";

@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    ShoppingListPage,
    SLOptionsPage,
    RecipesPage,
    ViewRecipePage,
    EditRecipePage,
    SignInPage,
    SignUpPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    ShoppingListPage,
    SLOptionsPage,
    RecipesPage,
    ViewRecipePage,
    EditRecipePage,
    SignInPage,
    SignUpPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    RecipeService,
    ShoppingListService,
    AuthService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
