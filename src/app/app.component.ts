import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from "../pages/tabs/tabs";
import { SignInPage } from "../pages/user/signin";
import { SignUpPage } from "../pages/user/signup";

import firebase from 'firebase';
import { AuthService } from "../service/auth";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = TabsPage;

  tabsPage = TabsPage;
  signInPage = SignInPage;
  signUpPage = SignUpPage;

  isAuthenticated =  false;

  @ViewChild('nav') nav : NavController;

  constructor(platform: Platform, 
    statusBar: StatusBar, 
    splashScreen: SplashScreen,
    private menuCtrl: MenuController,
    private authService: AuthService) {
    firebase.initializeApp({
      apiKey: "AIzaSyAxGANhz4ThHDpbNSScN1ibYY-TOV68Z0Y",
      authDomain: "ionic-recipe-book-5671f.firebaseapp.com"
    });
    firebase.auth().onAuthStateChanged(user => {
      this.isAuthenticated = user != null;
      if (user) {
        this.rootPage = this.tabsPage;
      } else {
        this.rootPage = this.signInPage;
      }
    });
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  onLoadPage(page) {
    this.nav.setRoot(page);
    this.menuCtrl.close();
  }

  onLogout() {
    this.authService.logout();
    this.menuCtrl.close();
    this.nav.setRoot(SignInPage);
  }
}

