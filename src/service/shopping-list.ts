import { OnInit, Injectable } from "@angular/core";
import { Ingredient } from "../data/ingredient";
import { AuthService } from "./auth";
import { Http, Response } from "@angular/http";
import "rxjs/Rx";

@Injectable()
export class ShoppingListService implements OnInit {
  ingredients = [];

  dbServerUrl = "https://ionic-recipe-book-5671f.firebaseio.com/";

  constructor(private http: Http, private authService: AuthService) {
    
  }

  ngOnInit() {
    const storedIngredients = localStorage.getItem("shoppingListIngredients");
    if (storedIngredients) {
        this.ingredients = JSON.parse(storedIngredients);
    }
  }

  getIngredients() {
    return this.ingredients.slice();
  }

  storeList(token: string) {
    const userId = this.authService.getActiveUser().uid;
    return this.http.put(this.dbServerUrl + userId + "/shopping-list.json?auth=" + token, this.ingredients)
      .map((response: Response) => {
        return response.json();
      });
  }

  fetchList(token: string) {
    const userId = this.authService.getActiveUser().uid;
    return this.http.get(this.dbServerUrl + userId + "/shopping-list.json?auth=" + token)
      .map((response: Response) => {
        return response.json();
      })
      .do((data)=> {
        this.ingredients = data
      });
  }

  addIngredients(newIngredients: Ingredient[]) {
    this.ingredients = this.ingredients.concat(newIngredients);
  }

  addIngredient(ingredient: Ingredient) {
    this.ingredients.push(ingredient);
    //this.storeData();
  }

  removeIngredients() {
    this.ingredients = [];
  }
}