import { Injectable } from "@angular/core";
import { Recipe } from "../data/recipe";
import uuidV4 from "uuid/v4";

@Injectable()
export class RecipeService {
  recipes: Recipe[];

  constructor() {
    const storedRecipes = localStorage.getItem("recipes");
    if (storedRecipes) {
      this.recipes = JSON.parse(storedRecipes);
    } else {
      this.recipes = [];
    }
  }

  storeData() {
    localStorage.setItem("recipes", JSON.stringify(this.recipes));
  }

  addRecipe(newRecipe: Recipe) {
    newRecipe.id = uuidV4();
    this.recipes.push(newRecipe);
    this.storeData();
  }

  updateRecipe(updatedRecipe: Recipe) {
    const position = this.recipes.findIndex((recipeParm: Recipe) => {
      return recipeParm.id == updatedRecipe.id;
    });
    this.recipes[position] = updatedRecipe;
    this.storeData();
  }

  deleteRecipe(recipeId: string) {
    const position = this.recipes.findIndex((recipeParm: Recipe) => {
      return recipeParm.id == recipeId;
    });
    this.recipes.splice(position, 1);
    this.storeData();
  }

  getRecipes():Recipe[] {
    return this.recipes.slice();
  }

  getRecipe(recipeId: string):Recipe {
    return this.recipes.find((recipeParm:Recipe) => {
      return recipeParm.id == recipeId;
    });
  }
}