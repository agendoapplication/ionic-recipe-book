import { Component } from '@angular/core';
import { ViewRecipePage } from "./view/recipe";
import { EditRecipePage } from "./edit/recipe";
import { RecipeService } from "../../service/recipe";
import { NavController } from "ionic-angular";

@Component({
  selector: 'page-recipes',
  templateUrl: 'recipes.html',
})
export class RecipesPage {
  recipes: any[];

  viewRecipePage = ViewRecipePage;

  constructor(private navCtrl: NavController, 
    private recipeService: RecipeService) {
  }

  ionViewDidEnter() {
    this.recipes = this.recipeService.getRecipes();
  }

  editRecipe() {
    this.navCtrl.push(EditRecipePage);
  }
}