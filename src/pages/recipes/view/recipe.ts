import { Component } from '@angular/core';
import { NavParams, NavController, AlertController } from 'ionic-angular';
import { Recipe } from "../../../data/recipe";
import { RecipeService } from "../../../service/recipe";
import { EditRecipePage } from "../edit/recipe";
import { ShoppingListService } from "../../../service/shopping-list";

@Component({
  selector: 'page-view-recipe',
  templateUrl: 'recipe.html',
})
export class ViewRecipePage  {
  recipe: Recipe;
  
  constructor(private navParams: NavParams, 
    private navCtrl: NavController,
    private recipeService: RecipeService,
    private alertCtrl: AlertController,
    private shoppingListService: ShoppingListService) {
    this.recipe = this.recipeService.getRecipe(navParams.data);
  }

  onAddIngredientsToShoppingList() {
    this.shoppingListService.addIngredients(this.recipe.ingredients);
  }

  onEditRecipe() {
    this.navCtrl.push(EditRecipePage, this.recipe.id);
  }

  onDeleteRecipe() {
    let confirm = this.alertCtrl.create({
      title: 'Delete this recipe?',
      message: `Do you want to delete ${this.recipe.title}?`,
      buttons: [
        {
          text: 'No',
          role: 'Cancel',
          handler: () => {
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.recipeService.deleteRecipe(this.recipe.id);
            this.navCtrl.pop();
          }
        }
      ]
    });
    confirm.present();
  }
}