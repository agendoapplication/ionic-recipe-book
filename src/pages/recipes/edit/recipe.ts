import { Component, OnInit } from '@angular/core';
import { NavParams, ActionSheetController, AlertController, NavController, ToastController } from 'ionic-angular';
import { Recipe } from "../../../data/recipe";
import { RecipeService } from "../../../service/recipe";
import { FormGroup, FormControl, Validators, FormArray } from "@angular/forms";

@Component({
  selector: 'page-edit-recipe',
  templateUrl: 'recipe.html',
})
export class EditRecipePage implements OnInit {
  toastDuration = 1500;

  recipe: Recipe;
  recipeForm: FormGroup;

  difficulties: string[] = [
    'Easy',
    'Medium',
    'Hard'
  ]

  constructor(private navParams: NavParams,
    private navCtrl: NavController,
    private actionSheetCtrl: ActionSheetController,
    private alertCtrl: AlertController,
    private recipeService: RecipeService,
    private toastCtrl: ToastController) {
  }

  ngOnInit() {
    const idParam = this.navParams.data;
    if (idParam && idParam.length) {
      this.recipe = this.recipeService.getRecipe(idParam);
    } else {
      this.recipe = {
        id: '',
        title: '',
        description: '',
        difficulty: '',
        ingredients: []
      }
    }
    this.initializeForm();
  }

  private initializeForm() {
    this.recipeForm = new FormGroup({
      'title': new FormControl(this.recipe.title, Validators.required),
      'description': new FormControl(this.recipe.description, Validators.required),
      'difficulty': new FormControl(this.recipe.difficulty || 'Medium', Validators.required),
      'ingredients': new FormArray([])
    });
    for (let ingredient of this.recipe.ingredients) {
      (<FormArray>this.recipeForm.get('ingredients'))
        .push(new FormControl(ingredient, Validators.required));
    }
  }

  onManageIngredients() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'What do you want do do?',
      buttons: [
        {
          text: 'Add Ingredient',
          handler: () => {
            this.showAddIngredientAlert();
          }
        },{
          text: 'Remove All Ingredients',
          role: 'destructive',
          handler: () => {
            const formArray = <FormArray>this.recipeForm.get('ingredients');
            if (formArray.controls.length > 0) {
              formArray.controls = [];
              const toast = this.toastCtrl.create({
                message: 'Ingredients removed',
                duration: this.toastDuration
              });
              toast.present();
            }
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();
  }

  showAddIngredientAlert() {
    let prompt = this.alertCtrl.create({
      title: 'Add Ingredient',
      inputs: [
        {
          name: 'name',
          placeholder: 'Name'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
          }
        },
        {
          text: 'Add',
          handler: data => {
            if (data.name == null || data.name.trim() == '') {
              const toast = this.toastCtrl.create({
                message: 'Please enter a valid value',
                duration: this.toastDuration
                //,position: 'bottom'
              });
              toast.present();
              return;
            }
            (<FormArray>this.recipeForm.get('ingredients'))
              .push(new FormControl(data.name, Validators.required));
              const toast = this.toastCtrl.create({
                message: 'Ingredient added',
                duration: this.toastDuration
              });
              toast.present();
          }
        }
      ]
    });
    prompt.present();
  }

  onSaveRecipe() {
    const formValue = this.recipeForm.value;
    let newIngredients = [];
    if (formValue.ingredients.length > 0) {
      newIngredients = formValue.ingredients.map(name => {
        return {name: name, amount: 1}
      });
    }
    const theRecipe: Recipe = {
      title: formValue.title,
      description: formValue.description,
      difficulty: formValue.difficulty,
      ingredients: newIngredients
    }
    if (this.recipe.id) {
      theRecipe.id = this.recipe.id;
      this.recipeService.updateRecipe(theRecipe);
    } else {
      this.recipeService.addRecipe(theRecipe);
    }
    this.recipeForm.reset();
    this.navCtrl.popToRoot();
  }
}