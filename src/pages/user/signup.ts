import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { NgForm } from "@angular/forms";
import { AuthService } from "../../service/auth";

@Component({
  selector: 'page-sign-up',
  templateUrl: 'signup.html',
})
export class SignUpPage {

  constructor(private navCtrl: NavController, 
    private navParams: NavParams,
    private authService: AuthService,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController) {
  }

  onSignUp(form: NgForm) {
    const loading = this.loadingCtrl.create({
      content: 'Sign you up...'
    });
    loading.present();
    let value = form.value;
    this.authService.signup(value.email, value.password)
      .then(
        data => {
          loading.dismiss();
        }
      )
      .catch(
        error => {
          loading.dismiss();
          const alert = this.alertCtrl.create({
            title: 'Sign up Failed!!',
            message: error.message,
            buttons: ['Ok']
          }
          );
          alert.present();
        }
      );
  }
}