import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { NgForm } from "@angular/forms";
import { AuthService } from "../../service/auth";

@Component({
  selector: 'page-sign-in',
  templateUrl: 'signin.html',
})
export class SignInPage {

  constructor(private navCtrl: NavController, 
    private navParams: NavParams,
    private authService: AuthService,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController) {
  }

  onSignIn(form: NgForm) {
    const loading = this.loadingCtrl.create({
      content: 'Signin you in...'
    });
    loading.present();
    const formValue = form.value;
    this.authService.signin(formValue.email, formValue.password)
      .then(data => {
        loading.dismiss();
      })
      .catch(error => {
        loading.dismiss();
        const alert = this.alertCtrl.create({
          title: 'Sign in failed!!',
          message: error.message,
          buttons: ['Ok']
        });
        alert.present();
      });
  }
}