import { Component } from '@angular/core';
import { NavController, NavParams, PopoverController } from 'ionic-angular';
import { NgForm } from "@angular/forms";
import { ShoppingListService } from "../../service/shopping-list";
import { Ingredient } from "../../data/ingredient";
import { SLOptionsPage } from "./sl-options/sl-options";
import { AuthService } from "../../service/auth";

@Component({
  selector: 'page-shopping-list',
  templateUrl: 'shopping-list.html',
})
export class ShoppingListPage {

  ingredients: Ingredient[];

  constructor(private navCtrl: NavController, 
    private navParams: NavParams,
    private shoppingListService: ShoppingListService,
    private popoverCtrl: PopoverController,
    private authService: AuthService) {
  }

  ionViewWillEnter() {
    this.refreshIngredients();
  }

  refreshIngredients() {
    this.ingredients = this.shoppingListService.getIngredients();
  }

  onAddItem(form: NgForm) {
    const ingredient: Ingredient = {
      name: form.value.name,
      amount: form.value.amount
    }
    this.shoppingListService.addIngredient(ingredient);
    this.refreshIngredients();
    form.reset();
  }

  onRemoveIngredients() {
    this.shoppingListService.removeIngredients();
    this.refreshIngredients();
  }

  onShowOptions(event: MouseEvent) {
    const popover = this.popoverCtrl.create(SLOptionsPage);
    popover.present({ev: event});
    popover.onDidDismiss(data => {
      if (data.action == 'load') {
        this.authService.getActiveUser().getToken()
          .then((token: string) => {
            this.shoppingListService.fetchList(token)
              .subscribe(
                (list: Ingredient[]) => {
                  if (list) {
                    this.ingredients = list;
                  }
                },
                error => {
                  console.log(error);
                }
              );
          });        
      } else { // store
        this.authService.getActiveUser().getToken()
          .then((token: string) => {
            this.shoppingListService.storeList(token)
              .subscribe(
                () => {
                  console.log("success")
                },
                error => {
                  console.log(error);
                }
              );
          });
      }
    });
  }
}