import { Ingredient } from "./ingredient";

export interface Recipe {
  id?: string;
  title: string;
  description: string;
  difficulty: string;
  ingredients: Ingredient[];
}